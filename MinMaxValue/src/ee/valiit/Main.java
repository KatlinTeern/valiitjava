package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int[] { 16, 4, 5, 3, 0, -4 };

	    int max = numbers[0];

        for (int i = 1; i < numbers.length ; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }

        System.out.println(max);

        int min = numbers[0];

        for (int i = 1; i < numbers.length ; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }

        System.out.println(min);

        // Leia suurim paaritu arv

//        int maxInt = Integer.MIN_VALUE;
////        int position = 1;
////        boolean oddNumbersFound = false;
////
////        for (int i = 1; i < numbers.length ; i++) {
////            if (numbers[i] % 2 != 0 && numbers[i] > maxInt) {
////                maxInt = numbers[i];
////                position = i + 1;
////                oddNumbersFound = true;
////            }
////        }
////
////        // Kui paarituid arve üldse ei ole, prindi, et "Paaritud arvud puuduvad"
////
////        if (!oddNumbersFound) {
////            System.out.printf("Suurim paaritu arv on %d ja selle positsioon on %d", maxInt, position);
////        } else {
////            System.out.println("Paaritud arvud puuduvad.");
////        }



        int maxInt = Integer.MIN_VALUE;
        int position = -1;

        for (int i = 1; i < numbers.length ; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > maxInt) {
                maxInt = numbers[i];
                position = i + 1;
            }
        }

        // Kui paarituid arve üldse ei ole, prindi, et "Paaritud arvud puuduvad"

        if (position != -1) {
            System.out.printf("Suurim paaritu arv on %d ja selle positsioon on %d", maxInt, position);
        } else {
            System.out.println("Paaritud arvud puuduvad.");
        }
    }
}

