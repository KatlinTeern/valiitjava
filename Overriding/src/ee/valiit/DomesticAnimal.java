package ee.valiit;

public class DomesticAnimal extends Animal {
    private String ownerName;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void gotToOwner() {
        System.out.println("Lähen omaniku juurde.");
    }
}
