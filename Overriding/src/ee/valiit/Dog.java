package ee.valiit;

public class Dog extends Pet {
    private boolean hasTail = true;

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }

    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s%n", cat.getName());
    }

    @Override
    public void eat() {
        System.out.println("Närin konti.");
    }

    @Override
    public int getAge() {
        int age = super.getAge();
        if (age == 0) {
            return 1;
        }
        return age;
    }

    @Override
    public double getWeight() {
        double weight = super.getWeight();
        if (weight == 0) {
            return 1;
        }
        return weight;
    }
}
