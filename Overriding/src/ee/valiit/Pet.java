package ee.valiit;

public class Pet extends DomesticAnimal {
    private String petName;

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public void haveInstagramPhotosTaken() {
        System.out.println("Omanik teeb must Instagrami jaoks pilti.");
    }

    @Override
    public void printInfo() {
        // super-iga saab otsida nö lähimat sugulast, otsib kõigepealt eelnevast klassist, siis
        // vaatab sellele eelnevat ja kui seal ka ei ole siis veel sellele eelnevat
        super.printInfo();
        System.out.printf("Hellitusnimi: %s%n", petName);
    }


}
