package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    // Method Overriding ehk meetodi ülekirjutamine
        // päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle

        Dog buldog = new Dog();
        buldog.eat();

        Cat siam = new Cat();
        siam.eat();
        siam.setPetName("Kiisuke");
        siam.printInfo();

        System.out.println(buldog.getAge());
        System.out.println(siam.getAge());

        // System.out.println(buldog.getName());

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks 1
        // Metsloomadel printInfo võiks kirjutada Nimi: metsloomal pole nime

        Elk elk = new Elk();
        elk.printInfo();

    }
}
