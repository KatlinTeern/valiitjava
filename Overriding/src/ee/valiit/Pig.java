package ee.valiit;

public class Pig extends FarmAnimal {
    private int targetWeight;

    public int getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(int targetWeight) {
        this.targetWeight = targetWeight;
    }

    public void gainWeight() {
        System.out.println("Võtan kaalus juurde.");
    }

}
