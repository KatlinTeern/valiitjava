package ee.valiit;

public class HerbivoreAnimal {
    private boolean hasHorns;

    public boolean isHasHorns() {
        return hasHorns;
    }

    public void setHasHorns(boolean hasHorns) {
        this.hasHorns = hasHorns;
    }

    public void watchOutForPreditors() {
        System.out.println("Olen vaenlaste suhtes valvas.");
    }

}
