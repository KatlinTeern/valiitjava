package com.javatpoint.dao; 
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
import com.javatpoint.beans.Emp;  
  
public class EmpDao {  
JdbcTemplate template;  
  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Emp p){  
	    String sql="insert into Emp99(name,salary,designation,age,trial,date) values('"+p.getName()+"',"+p.getSalary()+",'"
	    		+p.getDesignation()+"'," + p.getAge()+"," + p.getTrial()+",'"+dateTime()+"' "+ ")";  
	    return template.update(sql);  
	}  
	public int update(Emp p){  
	    String sql="update Emp99 set name='"+p.getName()+"', salary="+p.getSalary()+",designation='"
	    		+p.getDesignation()+"', age=" +p.getAge()+", trial=" +p.getTrial()+", date='"+p.getDate()+"'" + " where id="+p.getId()+"";  
	    return template.update(sql);  
	}  

	public int delete(int id){  
	    String sql="delete from Emp99 where id="+id+"";  
	    return template.update(sql);  
	}  
	public Emp getEmpById(int id){  
	    String sql="select * from Emp99 where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Emp>(Emp.class));  
	}  
	public List<Emp> getEmployees(){  
	    return template.query("select * from Emp99",new RowMapper<Emp>(){  
	        public Emp mapRow(ResultSet rs, int row) throws SQLException {  
	            Emp e=new Emp();  
	            e.setId(rs.getInt(1));  
	            e.setName(rs.getString(2));  
	            e.setSalary(rs.getFloat(3));  
	            e.setDesignation(rs.getString(4));  
	            e.setAge(rs.getInt(5));
	            e.setTrial(rs.getBoolean(6));
	            e.setDate(rs.getString(7)); 
	            return e;  
	        }  
	    });  
	}  
	
	public int getAverageEmployeeAge() {
		
		String sql= "SELECT CAST(ROUND(AVG(age))AS UNSIGNED) FROM emp99";
		int age = template.queryForObject(sql, Integer.class);
		return age;
	}
	/*
	private String dateTime() {
		
		String date = "SELECT DATE(NOW())";
		return date;
	}
	*/
	private String dateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
    	Calendar calendar = Calendar.getInstance();
    	Date date = calendar.getTime();
    
    	return dateFormat.format(date);
	}
	
	 
   
}  