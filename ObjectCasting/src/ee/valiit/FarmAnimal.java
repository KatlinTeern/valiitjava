package ee.valiit;

public class FarmAnimal extends DomesticAnimal {
    private int profit;

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    public void produceBenefits() {
        System.out.println("Olen inimesele kasulik.");
    }
}
