package ee.valiit;

public class Cat extends Pet {
    private boolean hasFur = true;

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    public void catchMouse() {
        System.out.println("Püüdsin hiire kinni.");
    }

    @Override
    public void eat() {
        System.out.println("Söön hiirt.");
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Karvade olemasolu: %s%n", hasFur);
    }

}
