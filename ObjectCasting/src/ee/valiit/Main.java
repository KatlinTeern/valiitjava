package ee.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int a = 100;
        short b = (short)a;

        a = b;

        double c = a;

        // Iga kassi võib võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // Iga loom ei ole kass
        // explicit casting
        Cat cat = (Cat)animal; // saab tagasi castida ainult siis kui on olnud sama loom (st. tglt animal on kass)
        cat.setName("Liisu");

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("Naki");
        Elk elk = new Elk();

        animals.add(dog);
        dog.setName("Muki");
        animals.add(elk);

        animals.add(new Elk());
        animals.get(animals.size() - 1).setWeight(100);

        animals.add(new Pet());
        animals.get(3).setName("Jaan");
        animals.get(animals.size() - 1).setName("Ott");

        animals.add(new WildAnimal());

        animals.get(animals.indexOf(dog)).setName("Peeter");

        dog.printInfo();
        System.out.println();

        // Kutsu kõikide loomade printInfo välja

        for (Animal animalInList  : animals) {
            animalInList.printInfo();
            animalInList.eat();
            System.out.println();
        }

        // siin on ainult animali parameetrid ja meetodid
        Animal secondAnimal = new Dog();
        // siin on dog´i parameetrid ja meetodid ka tagasi, aga peab teadma, et see oli enne kindlasti dog
        // ehk siis castime tagasi koeraks korraks et kasutada selle klassi parameetreid ja meetodeid
        ((Dog)secondAnimal).playWithCat(cat);
        ((Dog)secondAnimal).setHasTail(true);

        System.out.println();
        secondAnimal.printInfo();




    }
}
