package ee.valiit;

public class CarnivoreAnimal {
    private boolean canClimbTrees;

    public boolean isCanClimbTrees() {
        return canClimbTrees;
    }

    public void setCanClimbTrees(boolean canClimbTrees) {
        this.canClimbTrees = canClimbTrees;
    }

    public void chaseOtherAnimal() {
        System.out.println("Püüan teisi loomi.");
    }

}
