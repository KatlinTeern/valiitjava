package ee.valiit;

public class Main {

    public static void main(String[] args) {
        Cat angora = new Cat();
        angora.setName("Miisu");
        angora.setBreed("Angora");
        angora.setAge(2);
        angora.setWeight(3.34);

        angora.printInfo();
        angora.eat();

        System.out.println();

        Cat persian = new Cat();
        persian.setName("Täpi");
        persian.setBreed("Persian");
        persian.setAge(5);
        persian.setWeight(4.13);

        persian.printInfo();
        persian.eat();

        System.out.println();

        Dog tax = new Dog();
        tax.setName("Muki");
        tax.setBreed("Tax");
        tax.setAge(10);
        tax.setWeight(5.11);

        tax.printInfo();
        tax.eat();

        System.out.println();

        persian.catchMouse();
        tax.playWithCat(persian);
        System.out.println();

        // Lisa pärinevusahelast puuduvad klassid
        // Igale klassile lisa 1 muutuja (või parameeter) ja 1 meetod,
        // mis on ainult sellele klassile omane

        tax.setOwnerName("Leena");
        System.out.printf("%s omanik on %s.%n", tax.getName(), tax.getOwnerName());
        tax.setPetName("Nunnupall");
        System.out.printf("%s hellitusnimi on %s.%n", tax.getName(), tax.getPetName());
        tax.gotToOwner();
        tax.haveInstagramPhotosTaken();
        System.out.println();

        persian.setOwnerName("Linda");
        System.out.printf("%s omanik on %s.%n", persian.getName(), persian.getOwnerName());
        persian.setPetName("Tupsuke");
        System.out.printf("%s hellitusnimi on %s.%n", persian.getName(), persian.getPetName());
        persian.gotToOwner();
        persian.haveInstagramPhotosTaken();
        System.out.println();

        Pig pig = new Pig();
        pig.setName("Notsu");
        pig.setTargetWeight(150);
        System.out.printf("%s peab saavutama kaalu %dkg.%n", pig.getName(), pig.getTargetWeight());
        pig.gainWeight();
        pig.setOwnerName("Jaak");
        System.out.printf("%s omanik on %s.%n", pig.getName(), pig.getOwnerName());
        pig.gotToOwner();
        System.out.println();

        Cow cow = new Cow();
        cow.setName("Lehmu");
        cow.setAmountOfMilk(20);
        System.out.printf("%s annab päevas %d liitrit piima.%n", cow.getName(), cow.getAmountOfMilk());
        cow.giveMilk();
        cow.setOwnerName("Laine");
        System.out.printf("%s omanik on %s.%n", cow.getName(), cow.getOwnerName());
        cow.gotToOwner();
        System.out.println();



    }
}
