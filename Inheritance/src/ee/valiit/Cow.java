package ee.valiit;

public class Cow extends FarmAnimal {
    private int amountOfMilk;

    public int getAmountOfMilk() {
        return amountOfMilk;
    }

    public void setAmountOfMilk(int amountOfMilk) {
        this.amountOfMilk = amountOfMilk;
    }

    public void giveMilk() {
        System.out.println("Annan piima.");
    }
}
