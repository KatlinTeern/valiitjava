package ee.valiit;

public class WildAnimal extends Animal {
    private boolean livesInCave;

    public boolean isLivesInCave() {
        return livesInCave;
    }

    public void setLivesInCave(boolean livesInCave) {
        this.livesInCave = livesInCave;
    }

    public void wanderAroundWoods() {
        System.out.println("Jalutan metsas ringi.");
    }
}
