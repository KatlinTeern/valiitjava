package ee.valiit;

public class Pet extends DomesticAnimal {
    private String petName;

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public void haveInstagramPhotosTaken() {
        System.out.println("Omanik teeb must Instagrami jaoks pilti.");
    }

}
