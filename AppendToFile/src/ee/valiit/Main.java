package ee.valiit;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter("output.txt", true);
            fileWriter.append("Tere\r\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2

        int[] numbers = new int[] {1, 5, 6, 17, 2, 0, 8, 4, 45, 3};

        try {
            FileWriter fileWriter = new FileWriter("numbers.txt");
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] > 2) {
                    fileWriter.write(numbers[i] + System.lineSeparator());
                }
            }
            fileWriter.close();

        } catch (IOException e) {
            System.out.println("Faili ei leitud.");
        }

        // Küsi kasutajalt kaks arvu.
        // Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad)
        // ning nende summa ei ole paaris arv.

        Scanner scanner = new Scanner(System.in);
        int a = 0;
        int b = 0;
        boolean correctInput = false;

        do {
            try {

                System.out.println("Sisesta üks arv.");
                a = Integer.parseInt(scanner.nextLine());

                System.out.println("Sisesta teine arv.");
                b = Integer.parseInt(scanner.nextLine());

                System.out.printf("Arvude %d ja %d summa on %d.%n", a, b, a + b);

                correctInput = true;

            } catch (NumberFormatException e) {
                System.out.println("Viga sisestatud numbris.");
            }
        } while (!correctInput || (a + b) % 2 != 0);


        // Küsi kasutajalt mitu arvu ta tahab sisestada
        // seejärel küsi kasutajalt ühe kaupa need arvud (st. "Ütle arv" * 7)
        // ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde
        // Tulemus on iga programmi käivitamise järel on failis kõik eelnevad summad kirjas.

        System.out.println("\nMitu arvu sa soovid sisestada?");
        int answer = Integer.parseInt(scanner.nextLine());
        int sum = 0;

        for (int i = 0; i < answer; i++) {
            System.out.println("Sisesta arv.");
            int number = Integer.parseInt(scanner.nextLine());
            sum += number;
        }

        try {
            FileWriter fileWriter = new FileWriter("sum.txt", true);
            fileWriter.append(sum + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Faili ei leitud.");
        }


    }
}
