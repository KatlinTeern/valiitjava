package ee.valiit;

public class Main {

    public static void main(String[] args) {

        int sum = calculateSum(3, 2);
        System.out.printf("Arvude 3 ja 2 summa on %d%n", sum);

        System.out.printf("Tehte 6 - 5 vastus on %d%n", subtract(6, 5));
        System.out.printf("Tehte 6 * 5 vastus on %d%n", multiply(6, 5));
        System.out.printf("Tehte 6 / 5 vastus on %f%n", divide(6, 5));

        int[] numbers = new int[3];
        numbers[0] = 2;
        numbers[1] = 2;
        numbers[2] = 2;

        sum = sum(numbers);
        System.out.printf("Massiiivi arvude summa on %d%n", sum);

        numbers = new int[] { 2, 2, 45, 54 };
        sum = sum(new int[] {58, 25, 2, 3});
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        int[] reversed = reverseNumbers(numbers);
        printNumbers(reversed);
        // printNumbers(reverseNumbers(numbers));

        System.out.println();
        printNumbers(new int[] { 1, 2, 3, 4, 5, });

        System.out.println();
        printNumbers(reverseNumbers(new int[] { 1, 2, 3, 4, 5, }));

        System.out.println();
        printNumbers(convertToIntArray(new String[] { "2", "-12", "1", "0", "17" }));

        System.out.println();
        String[] numbersAsText = new String[] { "2", "-12", "1", "0", "17" };
        int[] numbersAsInt = convertToIntArray(numbersAsText);
        printNumbers(numbersAsInt);

        System.out.println();
        System.out.println(stringArrayToSentence(new String[] {"Toredat", "Java", "kursust", "meile"}));

        System.out.println();
        // String[] stringArrayToSentence = new String[] {"Toredat", "Java", "kursust", "meile"};
        // System.out.println(stringArrayToSentence(stringArrayToSentence, " "));
        System.out.println(stringArrayToSentence(new String[] {"Toredat", "Java", "kursust", "meile"}, " "));

        System.out.println("\nNumbrite keskmine summa on " + averageOfNumbers(new int[] {3, 2, 5}));

        System.out.println("\nArv a moodustab arvust b: " + percentCalculator(2, 4) + " %.");
        int a = 23;
        int b= 80;
        // Kui tahame kasutada % märki printf sees, siis kasutame topelt % ehk %%
        System.out.printf("Arv %d moodustab arvust %d: %.2f%%\n", a, b, percentCalculator(a, b));

        System.out.println("\nRingi ümbermõõt on " + circleCircumference(5));
        double radius = 10.25;
        System.out.printf("Ringi raadiusega %.2f ümbermõõt on %.2f%n", radius, circleCircumference(radius));

    }

    // Meetod, mis liidab 2 arvu kokku ja tagastab nende summa
    static int calculateSum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    static int subtract(int a, int b) {
        return a - b;
    }

    static int multiply (int a, int b) {
        return a * b;
    }

    static double divide (int a, int b) {
        return (double)a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemendid kokku ning tagastab summa

    static int sum(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Meetod, mis võtab parameetriks massiivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1

    static int[] reverseNumbers(int[] numbers) {
        int[] reverseNumbers = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            reverseNumbers[i] = numbers[numbers.length - i - 1];
        }
        return reverseNumbers;
    }

    // Meetod, mis prindib välja täisarvude massiivi elemndid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetriks Stringi massiivi (eeldusel, et tegelikult seal massiivis on
    // numbrid Stringidena)

    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }

    // Meetod, mis võtab parameetriks Stringi massiivi ning tagastab lause,
    // kus iga Stringi vahel on tühik

    static String stringArrayToSentence(String[] stringArray) {
        return String.join(" ", stringArray);
//        String sentence = String.join(" ", stringArray);
//        return sentence;
    }

    // Meetod, mis võtab parameetriks Stringi massiivi ning teine parameeter on sõnade eraldaja.
    // Tagastada lause.

    static String stringArrayToSentence(String[] stringArray, String wordJoiner) {
        return String.join(wordJoiner, stringArray);
//        String sentence = String.join(wordJoiner, stringArray);
//        return sentence;
    }

    // Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle

    static double averageOfNumbers(int[] numbers) {
        return sum(numbers) / (double)numbers.length;
//        int sum = 0;
//        double average = 0;
//        for (int i = 0; i < numbers.length; i++) {
//            sum += numbers[i];
//            average = (double)sum / numbers.length;
//        }
//        return average;
    }

    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.

    static double percentCalculator(int a, int b) {
        return (double) a / b * 100;
//        double percent = (double) a / b * 100;
//        return percent;
    }

    // Meetod, mis leiab ringi ümbermõõdu raadiuse järgi

    static double circleCircumference(double radius) {
        return 2 * Math.PI * radius;
//        double circumference = 2 * Math.PI * radius;
//        return circumference;
    }
}
