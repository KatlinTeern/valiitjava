package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    int a = -5;

	    // Kui arv on 4, siis prindi arv on 4
        // muul juhul kui arv on negatiivne,
        // siis kontrolli kas arv on suurem kui -10, prindi sellekohane tekst
        // muul juhul kontrolli kas arv on suurem kui 20, prindi sellekohane tekst

        if (a == 4) {
            System.out.println("Arv on 4.");
        }
        else {
            if (a < 0) {
                if (a < -10) {
                    System.out.println("Arv on väiksem kui -10.");
                }
            }
            else {
                if (a > 20) {
                    System.out.println("Arv on suurem kui 20.");
                }
            }
        }

    }
}
