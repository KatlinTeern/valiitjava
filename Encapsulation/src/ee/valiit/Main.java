package ee.valiit;

public class Main {

    public static void main(String[] args) {
        // int vaikeväärtus on 0
        // booleani vaikeväärtus on false
        // double vaikeväärtus on 0.0
        // float 0.0f
        // String vaikeväärtus on null
        // Objektide (Monitor, FileWriter) vaikeväärtus on null
        // int[] arvud vaikeväärtus on null

	    Monitor firstMonitor = new Monitor();
	    firstMonitor.setDiagonal(-1000);
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());

        firstMonitor.setManufacturer("Huawei");
        firstMonitor.printInfo();

        firstMonitor.setManufacturer("");
        firstMonitor.printInfo();



    }
}
