package ee.valiit;

public class Main {

    public static void main(String[] args) {
        float a = 123.3444f; // f-täht lõppu
        double b = 123.43432;

        System.out.printf("Arvude %e ja %e summa on %e%n", a, b, a + b);
        System.out.printf("Arvude %f ja %f summa on %f%n", a, b, a + b);
        System.out.printf("Arvude %g ja %g summa on %g%n", a, b, a + b);

        float c = 130;
        float e = 9;

        System.out.println(c / e);
        System.out.println(c * e);

        double f = 130;
        double g = 9;

        System.out.println(f / g);
        System.out.println(f * g);
    }
}
