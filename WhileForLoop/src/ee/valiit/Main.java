package ee.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        for (int i = 1; i <= 5; i++) {
//            System.out.println(i);
//        }

        int i = 1;

        while (i <= 5) {
            System.out.println(i);
            i++;
        }

        // Küsi kasutajalt, mis päev täna on
        // seni kuni ta ära arvab


        Scanner scanner = new Scanner(System.in);
        String answer = "";

//        while (!answer.toLowerCase().equals("neljapäev")) {
//            System.out.println("Mis päev täna on?");
//            answer = scanner.nextLine();
//        }


        // for loop kinf of võrdub while loop
        // aint kaks komponenti rohkem for loopis
        for (;!answer.toLowerCase().equals("neljapäev");) {
            System.out.println("Mis päev täna on?");
            answer = scanner.nextLine();
        }


    }
}
