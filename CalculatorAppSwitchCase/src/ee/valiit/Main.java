package ee.valiit;

import java.util.Scanner;

public class Main {

    // Küsi kasutajalt kaks arvu
    // Seejärel küsi kasutajalt, mis tehet ta soovib teha
    // liitmine, lahutamine, korrutamine, jagamine
    // Prindi kasutajale tehte vastus
    // Tee tsükliline kordus ka

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        boolean wrongAnswer;

        do {
            System.out.println("Siseta üks arv.");
            int a = Integer.parseInt(scanner.nextLine());

            System.out.println("Siseta teine arv.");
            int b = Integer.parseInt(scanner.nextLine());

            do {
                System.out.println("Vali tehe: \na liitmine, \nb lahutamine, \nc korrutamine, \nd jagamine ");
                String userChoice = scanner.nextLine();

                wrongAnswer = false;

                // Switch case konstruktsioon sobib kasutamiseks if ja else if asemel siis,
                // kui if ja else if kontrollivad ühe ja sama muutuja väärtust

                switch (userChoice) {
                    case "A":
                    case "a":
                        System.out.printf("Arvude %d ja %d summa on %d.%n", a, b, sum(a, b));
                        break;
                    case "B":
                    case "b":
                        System.out.printf("Arvude %d ja %d vahe on %d.%n", a, b, subtract(a, b));
                        break;
                    case "C":
                    case "c":
                        System.out.printf("Arvude %d ja %d korrutis on %d.%n", a, b, multiply(a, b));
                        break;
                    case "D":
                    case "d":
                        System.out.printf("Arvude %d ja %d jagatis on %.2f.%n", a, b, divide(a, b));
                        break;
                    default:
                        System.out.println("Selline valik puudub.");
                        wrongAnswer = true;
                        break;
                }


            } while (wrongAnswer);

            System.out.println("Kas soovid jätkata? jah/ei");

        } while (scanner.nextLine().equals("jah"));



    }

    static int sum(int a, int b) {
        return a + b;
    }

    static int subtract(int a, int b) {
        return a - b;
    }

    static int multiply (int a, int b) {
        return a * b;
    }

    static double divide (int a, int b) {
        return (double)a / b;
    }




}
