package ee.valiit;

public class Main {

    public static void main(String[] args) {

        // Twodimensional array table
        int twoDm[][]= new int[5][7];

        for(int i=0;i<5;i++){
            for(int j=0;j<7;j++) {
                System.out.print(twoDm[i][j]+" ");
            }
            System.out.println("");
        }
    }
}
