package ee.valiit;

import java.io.*;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
        // Loe failist input.txt iga teine rida ning kirjuta need read
        // faili output.txt

        try {
            // Notepad vaikimisi kasutab ANSI encodingut. Selleks, et FileReader oskaks
            // seda korrektselt lugeda (täpitähti), peame talle ette ütlema, et
            // loe seda ANSI encodingus
            // Cp1252 on javas ANSI encoding
            // Teine variant on kohe algul Notepadis UTF-8 seaded valida (pigem soovitav alati)
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // Faili kirjutades on javas vaikeväärtus UTF-8
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt", Charset.forName("UTF-8"));

            String line = bufferedReader.readLine();
            int lineNumber = 1;

            while (line != null) {
                if (lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator());
                }
                line = bufferedReader.readLine();
                lineNumber++;
            }

            fileWriter.close();
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

