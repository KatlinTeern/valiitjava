package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    double a = Double.parseDouble("12.12");
	    String number = "123.2233";
	    double b = Double.parseDouble(number);

        System.out.println(a);
        System.out.println(b);

        number = String.valueOf(a);
        System.out.println(number);
        long c = 3400000000000L;

        number = String.valueOf(c);
        System.out.println(number);

        number = "-2";

        byte d = Byte.parseByte(number);
        System.out.println(d);

        short e = Short.parseShort(number);
        System.out.println(e);

        float f = Float.parseFloat(number);
        System.out.println(f);
    }
}
