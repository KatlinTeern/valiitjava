package ee.valiit;

public class Main {

    public static void main(String[] args) {
    // Casting on teisendamine ühest arvutüübist teise
        byte a = 3;

        // Kui üks numbritüüp manhub teise sisse, siis toimub
        // automaatne teisendamine
        // implicit casting
        short b = a;

        // Kui üks numbritüüp ei pruugi mahtuda teise sisse,
        // siis peab kõigepealt ise veenduma, et see number,
        // mahub sinna teise numbritüüpi
        // ja kui mahub, siis peab ise seda teisendama
        // explicit casting

        short c = 300;
        byte d = (byte) c;

        System.out.println(d);

        long e = 10000000000L;
        int f = (int) e;

        System.out.println(f);

        long g = f;
        g = c;
        g = d;

        float h = 123.23424F;
        double i = h;

        double j = 55.15122415144856558;
        float k = (float) j;

        System.out.println(k);

        double l = 12E50; // 12 * 10^50
        float m = (float) l;

        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;

        System.out.println(q);

        int r = 2;
        double s = 9;
        // int / int = int
        // int / double = double
        // double / int = double
        // double + int = double
        // double / float = double

        System.out.println(r / (double)s);

        System.out.println(r / (float)s);

        float t = 12.55555F;
        double u = 23.555555;
        System.out.println(t / u);


    }
}
