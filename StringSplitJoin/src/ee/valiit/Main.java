package ee.valiit;

public class Main {

    public static void main(String[] args) {
        String word = "kala";
        int length = word.length();
        System.out.println(length);

        String[] words = new String[] { "Põdral", "maja", "metsa", "sees" };

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        System.out.println();

        // Prindi kõik sõnad massiivist, mis algavad m-tähega
        for (int i = 0; i < words.length; i++) {
            if (words[i].substring(0, 1).toLowerCase().equals("m")) {
                System.out.println(words[i]);
            }
        }

        System.out.println();

        // Prindi kõik a-tähega lõppevad sõnad
        for (int i = 0; i < words.length; i++) {
            if (words[i].substring(words[i].length() - 1).toLowerCase().equals("a")) {
                //substring vaatab lõpuni, teist parameetrit ei ole tegelikult vaja
                System.out.println(words[i]);
            }
        }

        System.out.println();

        // Loe üle kõik sõnad, mis sisaldavad a tähte ja prindi välja arv

        int wordCount = 0;

        for (int i = 0; i < words.length; i++) {
            if (words[i].indexOf("a") != -1) {
                // System.out.println(words[i]);
                wordCount++;
            }
        }
        System.out.println(wordCount);

        System.out.println();

        // Prindi välja kõik sõnad, kus on 4 tähte

        for (int i = 0; i < words.length; i++) {
            if (words[i].length() == 4) {
                System.out.println(words[i]);
            }
        }

        System.out.println();

        // Prindi välja kõige pikem sõna

        String longestWord = words[0];

        for (int i = 0; i < words.length; i++) {
            if (words[i].length() > longestWord.length()) {
                longestWord = words[i];
            }
        }
        System.out.println(longestWord);

        System.out.println();

        // Prindi välja sõna, kus on esimene ja viimane täht on sama

        for (int i = 0; i < words.length; i++) {
            if (words[i].substring(0, 1).toLowerCase().equals
                    (words[i].substring(words[i].length() - 1).toLowerCase())) {
                System.out.println(words[i]);
            }
        }



    }
}
