package ee.valiit;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        // Try plokis otsitakse/oodatakse Exceptioneid (Erind, Erand, Viga)
        try {
            // FileWriter on selline klass, mis tegeleb faili kirjutamisega
            // sellest klassist objekti loomisel antakse talle ette faili asukoht
            // faili asukoht võib olla ainult faili nimega kirjeldatud: output.txt
            // sel juhul kirjutatakse faili, mis asub samas kaustas kus meie Main.class
            // või täispika asukohaga c:\\users\\opilane\\documents\\output.txt
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");

            fileWriter.write(String.format("Elas metsas Mutionu%n"));
            fileWriter.write("keset kuuski noori vanu" + System.lineSeparator());
            fileWriter.write("kadak-põõsa juure all\r\n");

            fileWriter.close();

        // Catch polkis püütakse kinni kindlat tüüpi Exception või kõik exceptionid mis
        // pärinevad antud exceptionist
        } catch (IOException e) {
            // printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise
            // hierarhia/ajalugu
            // e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik.");
        }
    }
}
