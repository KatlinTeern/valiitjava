package ee.valiit;

public class Point {
    public int x;
    public int y;

    public static void printStatic() {
        System.out.println("Olen static punkt.");
    }

    public void printNotStatic() {
        System.out.println("Olen punkt.");
    }

    public void increment() {
        x++;
        y++;
    }
}
