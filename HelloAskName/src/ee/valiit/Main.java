package ee.valiit;

// import tähendab, et antud klassile Main lisatakse ligipääs java class library paketile
// java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Loome Scanner tüübist objekti nimega Scanner
        // mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis on sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Mis on sinu lemmikvärv?");
        String color = scanner.nextLine();

        System.out.println("Mis autoga sa sõidad?");
        String car = scanner.nextLine();

        System.out.println("Sinu nimi on " + name + ". Su lemmikvärv on " + color + ". Sa sõidad " + car + ".");

        System.out.printf("Sinu nimi on %s. Su lemmikvärv on %s. Sa sõidad %s.\n",
                name, color, car);

        // liidab Stringid kokku

        StringBuilder builder = new StringBuilder();
        builder.append("Sinu nimi on ");
        builder.append(name);
        builder.append(". Su lemmikvärv on ");
        builder.append(color);
        builder.append(". Sa sõidad ");
        builder.append(car);
        builder.append(".");

        String fullText = builder.toString();
        System.out.println(fullText);
        // System.out.println(builder.toString());

        // liidab teksti kokku, aga ei prindi välja (saab nt hiljem meilile saata vm)
        // Nii System.out.printf kui ka String.format kasutavad enda siseselt Stringbuilderit

        String text = String.format("Sinu nimi on %s. Su lemmikvärv on %s. Sa sõidad %s.\n",
                name, color, car);
        System.out.println(text);
    }
}
