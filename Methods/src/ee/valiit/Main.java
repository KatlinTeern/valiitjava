package ee.valiit;

public class Main {

    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud
    // kindlat funksionaalsust.

    // Kui koodis on korduvaid koodi osasid, võiks mõelda,
    // et äkki peaks nende kota tegema eraldi meetodi

    public static void main(String[] args) {
        printHello();
        printHello(3);
        printText("Väljas on ilus ilm.");
        printText("Päikest ja head tuju kõigile!", 2);
        printText(2018, "Eelmine aasta");
        printText("Heihei", 3, true);


    }

    // Lisame meetodi, mis prindib ekraanile Hello

    private static void printHello() {
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi

    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }
    }

    // Lisame meetodi, mis prindib etteantud teksti välja
    // printText

    static void printText(String text) {
        System.out.println(text);
    }

    // Lisame meetodi,
    // mis prindib etteantud teksti välja etteantud arv kordi

    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }
    }

    static void printText(int year, String text) {
        System.out.printf("%d: %s%n", year, text);
    }

    // Method OVERLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite kombinatsiooniga
    // Meetodi ülelaadimine


    // Lisame meetodi,
    // mis prindib etteantud teksti välja etteantud arv kordi
    // lisaks saab öelda, kas tahame teha kõik tähed enne suurteks või mitte

    static void printText(String text, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if (toUpperCase) {
                System.out.println(text.toUpperCase());
            }else {
                System.out.println(text);
            }
        }
    }



}
