﻿-- See on lihtne hello world teksti päring, mis tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World

SELECT 'Hello World';

-- Täisarvu saab küsida ilma '' märkideta
SELECT 3;

-- ei tööta PostgreSQL´is. Nt. MSSql´is töötab
SELECT 'raud' + 'tee'; 

-- Standard CONCAT töötab kõigis erinevates SQL serverites
SELECT CONCAT ('all', 'maa', 'raud', 'tee', 'jaam', '.', 2, 0, 0, 4);

-- Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'

-- AS märksõnaga saab anda antud veerule nime
SELECT 
	'Peeter' AS eesnimi,
	'Paat' AS perekonnanimi, 
	23 AS vanus, 
	75.45 AS kaal, 
	'Blond' AS juuksevärv

-- Tagastab praeguse kellaaja ja kuupäeva mingis vaikimisi formaadis
SELECT NOW();

-- Kui tahan konkreetset osa sellest, nt. aastat või kuud
SELECT date_part('year', NOW());

-- Kui tahan mingit kuupäeva osa ise etteähtud kuupäevast
SELECT date_part('month', TIMESTAMP '2019-01-01')

-- Kui tahan kellaajast saada nt minuteid
SELECT date_part('minutes', TIME '10:10')

-- Kuupäeva formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY');


/*
-- teksti puhul nii vist
SELECT varchar(5);

float(n) -- täpsus
*/

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT NOW() + interval '1 day ago';
SELECT NOW() - interval '1 day';
SELECT NOW() - interval '7 years';
SELECT NOW() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds';

-- Tabeli loomine
CREATE TABLE student (
	id serial PRIMARY KEY, 
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- ei tohi tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL, -- tohib tühi olla
	weight numeric(5, 2) NULL,
	birthday date NULL
);

-- Täisnime küsimine
SELECT CONCAT(first_name, ' ', last_name) AS täisnimi FROM student;

-- Tabelist kõikide ridade kõikide veergude küsimine
-- * siit tähendab, et anna kõik veerud
SELECT * FROM student;


SELECT 
	first_name, 
	last_name 
FROM 
	student;
	

SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi
FROM 
	student;
	
-- Vanuse määramine
SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
	date_part('year', NOW()) - date_part('year', birthday) AS vanus
FROM 
	student;
	
-- Kui tahad otsida/filtreerida mingi tingimuse järgi, siis WHERE on see märksõna
SELECT 
	*
FROM 
	student
WHERE
	height = 180;	
	
-- && asemel on siin AND ja || asemel OR	
SELECT 
	*
FROM 
	student
WHERE
	first_name = 'Peeter' 
	AND last_name = 'Tamm';
	
-- Küsi tabelist eesnime ja perekonnanime järgi 
-- mingi Peeter Tamm ja mingi Mari Maasikas	

SELECT 
	*
FROM 
	student
WHERE
	(first_name = 'Mari' AND last_name = 'Maasikas')
	OR 
	(first_name = 'Peeter' AND last_name = 'Puujalg')


-- Anna mulle õpilased, kelle pikkus jääb 170-180 cm vahele

SELECT 
	*
FROM 
	student
WHERE
	height >= 170 AND height <= 180


-- Anna mulle õpilased, kes on pikemad kui 170 cm või lühemad kui 150 cm

SELECT 
	*
FROM 
	student
WHERE
	height >= 170 OR height <= 150

-- Anna mulle õpilaste eesnimi ja pikkus, kelle on sünnipäev jaanuaris	

SELECT 
	first_name, height
FROM 
	student
WHERE
	date_part('month', birthday) = 1
	
-- Anna mulle õpilased, kelle middle_name ja kaal on null(määramata)
-- NULLiga võrdlemine siin on IS	

SELECT 
	*
FROM 
	student
WHERE
	middle_name IS NULL AND weight IS NULL
	
-- Anna mulle õpilased, kelle middle_name ei ole null(määramata)
	
SELECT 
	*
FROM 
	student
WHERE
	middle_name IS NOT NULL	
	
-- Anna mulle õpilased, kelle pikkus ei ole 180 cm
SELECT 
	*
FROM 
	student
WHERE
	height <> 180
	-- height != 180
	-- NOT (height = 180)

-- Anna mulle õpilased, kelle pikkus on 169, 140 või 180
SELECT 
	*
FROM 
	student
WHERE
	-- height = 169 OR height = 140 OR height = 180
	height IN (169, 140, 180)

-- Anna mulle õpilased, kelle eesnimi on Peeter, Mari või Kalle
SELECT 
	*
FROM 
	student
WHERE
	first_name IN ('Peeter', 'Mari', 'Kalle')
	
-- Anna mulle õpilased, kelle eesnimi ei ole Peeter, Mari või Kalle
SELECT 
	*
FROM 
	student
WHERE
	first_name NOT IN ('Peeter', 'Mari', 'Kalle')	
	
-- Anna mulle õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitmes päev
SELECT 
	*
FROM 
	student
WHERE
	date_part('day', birthday) IN (1, 4, 7)	
	
-- Kõik WHERE võrdlused jätavad välja NULL väärtusega read
SELECT 
	*
FROM 
	student
WHERE
	height != 180
	
-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks
SELECT 
	*
FROM 
	student
ORDER BY
	height

-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks
-- Kui pikkused on võrdsed, järjesta kaalu järgi
SELECT 
	*
FROM 
	student
ORDER BY
	height, weight

-- Kui tahan tagurpidises järjekorras, siis lisanub sõna DESC (Descending)
-- Tegelikult ASC (Ascending), mis on vaikeväärtus
SELECT 
	*
FROM 
	student
ORDER BY
	first_name DESC, last_name DESC
	
-- Viimati lisatud Peeter on eespool	
SELECT 
	*
FROM 
	student
ORDER BY
	first_name, id DESC	
	
-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused, mis jäävad 160 ja 170 vahele
SELECT 
	height
FROM 
	student
WHERE
	height > 160 OR height < 170
ORDER BY
	birthday

-- Anna mulle kõik P-ga algavad sõnad (suur- ja väiketähed on olulised)
SELECT 
	*
FROM 
	student
WHERE
	first_name LIKE 'P%'
	-- first_name LIKE '%r' - lõppeb r-ga
	-- first_name LIKE '%s%' - sisaldab s-i
	
SELECT 
	*
FROM 
	student
WHERE
	first_name LIKE 'Ka%' -- eesnimi algab 'Ka'
	OR first_name LIKE '%ee%' -- eesnimi sisaldab 'ee'
	OR first_name LIKE '%lle' -- eesnimi lõppeb 'lle'

INSERT INTO student
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Tiina', 'Tihane', 155, 55.32, '1994-11-11', NULL),
	('Alar', 'Allikas', 185, 92.65, '1989-07-20', NULL),
	('Liina', 'Mägi', 165, 67.55, '1987-03-13', 'Liisu')

-- Tabelis kirje muutmine
-- UPDATE lausega peab olema ETTEVAATLIK
-- alati peab kasutama WHERE-i lause lõpus

UPDATE 
	student
SET	
	height =173
WHERE
	first_name = 'Kalle' AND last_name = 'Kalamees'
	-- id = 4
	
	
UPDATE 
	student
SET	
	height = 150,
	weight = 55.13,
	middle_name = 'Sille',
	birthday = '1998-04-05'
WHERE
	id = 8	
	
-- Muuda kõigi õpilaste pikkus ühe võrra suuremaks
UPDATE 
	student
SET	
	height = height + 1
	
-- Suurenda hiljem kui 1999 sündinud õpilastel sünnipäeva ühe päeva võrra
UPDATE 
	student
SET	
	birthday = birthday + interval '1 day'
WHERE	
	date_part('year', birthday) > 1999

-- Kustutamisega olla ettevaatlik. ALATI KASUTA WHERE´i.
DELETE FROM
	student
WHERE 
	id = 7

-- Andmete CRUD operations
-- Create(Insert), Read(Select), Update, Delete	

-- Loo uus tabel "loan", millel on väljad: amount(reaalarv), start_date, due_date, student_id
CREATE TABLE loan (
	id serial PRIMARY KEY, 
	amount numeric(11,2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);

-- Lisa neljale õpilasele laenud
-- Kahele neist lisa veel 1 laen
INSERT INTO loan
	(amount, start_date, due_date, student_id)
VALUES
	(2000, NOW(), '2020-02-14', 2),
	(1500, NOW(), '2020-05-14', 2),
	(1750, NOW(), '2020-02-14', 11),
	(2500, NOW(), '2021-07-14', 11),
	(750, NOW(), '2020-02-14', 10),
	(500, NOW(), NOW() + interval '1 year', 5)
	
-- Anna mulle kõik õpilased koos oma laenudega
SELECT
	*
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
	
-- Anna mulle kõik õpilased koos oma laenudega
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read
-- kus on võrdsed student.id = loan.student.id
-- ehk need read, kus tabelite vahel on seos.
-- Ülejäänud read ignoreeritakse.
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
INNER JOIN -- INNER JOIN on vaikimisi JOIN, inner sõna võib ära jätta
	loan
	ON student.id = loan.student_id

-- Anna mulle kõik õpilased koos oma laenudega
-- aga ainult sellised, mis on suuremad, kui 1000.00
-- järjesta laenu summa järgi suuremast väiksemaks
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student	
JOIN
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 1000
ORDER BY 
	loan.amount DESC	
	
-- Loo uus tabel loan_type, milles väljad name ja description
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);

-- Uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

-- Tabeli kustutamine
DROP TABLE student;

-- Lisame mõned laenutüübid
INSERT INTO loan_type
	(name, description)
VALUES
	('Õppelaen', 'See on väga hea laen'),
	('SMS laen', 'See on väga väga halb laen'),
	('Väikelaen', 'See on kõrge intressiga laen'),
	('Kodulaen', 'See on madala intressiga laen')	
	
-- Kolme tabeli Inner-join
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- INNER JOIN puhul joinimiste järjekord ei ole oluline	
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON lt.id = l.loan_type_id
JOIN
	student AS s
	ON s.id = l.student_id	
	
-- Kolmas variant
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
JOIN
	student AS s
	ON s.id = l.student_id
	
-- LEFT JOIN puhul võetakse joini esimesest (VASAKUST) tabelist kõik read
-- ning teisest (Paremas) tabelis näidatakse puuduvatel kohtadel NULL 	
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
	
--SQLite - enamus app-id kasutavad seda andmete hoidmiseks; väike andmebaas; Chrome nt. hoiab history´t seal


-- LEFT JOIN puhul on järjekord väga oluline
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id	
	
-- Annab kõik kombinatsioonid kahe tabeli vahel
SELECT
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name	
	
-- FULL OUTER JOIN on sama mis LEFT + RIGHT JOIN kokku	
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id	
	
-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu 
-- ja kelle laenu kogus on üle 100 euro	
-- Tulemused järjesta laenu võtja vanuse järgi 
-- väiksemast suuremaks (nooremast vanemaks)
SELECT
	s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE
	lt.name = 'SMS laen' 
	AND l.amount > 100
ORDER BY
	s.birthday DESC
	
-- Aggregate functions	
-- Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses
-- küsida mingit muud välja tabelist, sest agregaatfunktsiooni tulemus on alati ainult 1 number
-- ja seda ei saa kuidagi näidata koos väljadega,
-- mida võib olla mitu rida


-- Keskmise leidmine. Jäetakse välja read, kus height on NULL
SELECT
	AVG(height)
FROM
	student

SELECT
	ROUND(AVG(height), 0)
FROM
	student
	
-- Palju on üks õpilane keskmiselt laenu võtnud
-- Arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL)
SELECT
	AVG(COALESCE(loan.amount, 0)) -- sulgudes: see mis on ja see mis asemele pannakse
FROM
	student	
LEFT JOIN
	loan
	ON student.id = loan.student_id	


SELECT
	-- ROUND võtab esimeseks parameetriks võtab, mida ümardada, ja teiseks, mitu komakohta
	ROUND(AVG(COALESCE(loan.amount, 0)), 0) AS "Keskmine laenusumma",
	MIN(loan.amount) "Minimaalne laenusumma",
	MAX(loan.amount) "Maksimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel õpilasel on pikkus"
FROM
	student	
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
	
-- Kasutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on
-- GROUP BY´s ära toodud (s.first_name, s.last_name)
-- Teisi välju saab kasutada ainult agregaatfunktsioonide sees
SELECT
	s.first_name, 
	s.last_name, 
	AVG(l.amount),
	SUM(l.amount),
	MIN(l.amount),
	MAX(l.amount)	
FROM
	student AS s
JOIN	
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name

-- Anna mulle laenude summad laenutüüpide järgi	
SELECT
	lt.name, SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	lt.name
	

-- Anna mulle laenude summad sünniaastate järgi
SELECT
	date_part ('year', s.birthday), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part ('year', s.birthday)
	
	
-- Anna mulle laenude, mis ületavad 1000, summad sünniaastate järgi
SELECT
	date_part ('year', s.birthday), SUM(l.amount)
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part ('year', s.birthday)
-- HAVING on nagu WHERE, aga peale GROUP BY kasutust
-- Filtreerimisel saab kasutada ainult neid välju, mis on
-- GROUP BY´s ja agregaatfunktsioonides
HAVING	
	date_part ('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000 -- anna ainult need read, kus summa oli suurem 1000
	AND COUNT(l.amount) = 2 -- anna ainult need read, kus laene kokku oli 2
ORDER BY	
	date_part ('year', s.birthday)


-- Anna mulle laenude summad grupeerituna õpilase ning laenutüübi kaupa	
-- Mari õppelaen 2400 (1200+2400)
-- Mari väikelaen 1000
-- Jüri SMS laen 500
SELECT
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name


-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summad
-- SMS laen 2 1000
-- Kodulaen 1 2300
SELECT
	lt.name, COUNT(lt.amount), SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	lt.name
	
-- Mis aastal sündinud võtsid kõige suurema summa laenu?	
SELECT
	date_part('year', s.birthday), SUM(l.amount)
FROM
	loan AS l
JOIN
	student AS s
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)
ORDER BY 
	SUM(l.amount) DESC
LIMIT 1	

-- Anna mulle õpilaste eesnime esitähe esinemise statistika
-- ehk siis mitu õpilase eesnime algab mingi tähega	
-- M 3
-- A 2
-- K 1
-- SUBSTRING: mis sõnast, mitmendast tähest ja mitu tähte algab 1st 
SELECT 
	SUBSTRING (first_name, 1, 1), COUNT(first_name)
FROM
	student
GROUP BY
	SUBSTRING (first_name, 1, 1)
	
	
-- nii saab tühiku asukoha	
SELECT POSITION(' ' IN 'katlin teern') 
	
-- nii saab esimese sõna	
SELECT SUBSTRING('katlin teern', 1, POSITION(' ' IN 'katlin teern')) 

-- nii saab teise sõna
SELECT SUBSTRING('katlin teern', 
				 POSITION(' ' IN 'katlin teern') + 1)
				 

-- saab ainult teise sõna
SELECT SUBSTRING ('katlin teern', 7) 				 


-- Subquery, Inner query või Nested query - üks päring on teise sees, see on aeglasem kui tavaline päring,
-- kasuta pigem harvem
-- Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT
	first_name, last_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student)	
	

-- Anna mulle õpilased, kelle eesnimi on keskmise pikkusega õpilaste keskmine nimi
SELECT
	first_name, last_name
FROM
	student
WHERE
	first_name IN
(SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student))


-- Lisa kaks vanimat õpilast töötajate tabelisse
INSERT INTO employee (first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMIT 2	


-- Anna mulle kõik matemaatikat õppivad õpilased
SELECT
	s.first_name, s.last_name, su.name
FROM
	student AS s
JOIN
	student_subject AS ss
	ON s.id = ss.student_id
JOIN
	subject AS su
	ON su.id = ss.subject_id
WHERE
	su.name = 'Matemaatika'


-- Anna mulle kõik õppeained, kus Mari õpib
SELECT
	s.first_name, s.last_name, su.name
FROM
	student AS s
JOIN
	student_subject AS ss
	ON s.id = ss.student_id
JOIN
	subject AS su
	ON su.id = ss.subject_id
WHERE
	s.first_name = 'Mari'
	
	
MySQL

Project > Configure > Convert to Maven Project -- pom.xml fail avaneb
https://www.javatpoint.com/spring-mvc-crud-example	
	
-- Teisendamine täisarvuks
SELECT CAST(ROUND(AVG(age))AS UNSIGNED) FROM test.emp99	

DATE_FORMAT(date,'%d/%m/%Y')
	