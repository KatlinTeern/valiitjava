package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    Car bmw = new Car();
	    bmw.startEngine();
	    bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();

        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);

        Car fiat = new Car();
        Car mercedes = new Car();
        Car opel = new Car("Opel", "Vectra", 1999, 205);
        opel.startEngine();
        opel.accelerate(80);

        Car secondOpel = new Car("Opel", "Vectra", 1999, 205, Fuel.DIESEL, false);
        secondOpel.startEngine();
        secondOpel.accelerate(80);
        secondOpel.park();

        Person person = new Person("Kätlin", "Teern", Gender.FEMALE, 32);
        Person secondPerson = new Person("John", "Smith", Gender.MALE, 35);
        Person thirdPerson = new Person("Helen", "Must", Gender.FEMALE, 25);
        Person fourthPerson = new Person("Tiina", "Sinine", Gender.FEMALE, 50);
        Person fifthPerson = new Person("Peeter", "Tamm", Gender.MALE, 42);
        Person sixthPerson = new Person("Adam", "Smith", Gender.MALE, 38);

        Car renault = new Car("Renault", "Clio", 2005, 200);
        renault.setOwner(person);
        renault.setDriver(person);
        renault.setMaxPassengers(5);

        System.out.printf("%s omaniku vanus on %d%n", renault.getMake(), renault.getOwner().getAge());

        renault.addPassengers(person);
        renault.addPassengers(secondPerson);
        renault.addPassengers(thirdPerson);
        renault.addPassengers(fourthPerson);
        renault.addPassengers(fifthPerson);
        renault.addPassengers(sixthPerson);

        System.out.println();

        renault.showPassengers();

        System.out.println();

        renault.removePassengers(fifthPerson);
        renault.addPassengers(sixthPerson);
        System.out.println();

        renault.showPassengers();
        System.out.println();

        renault.removePassengers(person);
        System.out.println();

//        System.out.println("Autos on järgmised reisijad: ");
//        for (int i = 0; i < renault.getPassengers().size(); i++) {
//            System.out.println(renault.getPassengers().get(i).getFirstName());
//        }








    }
}
