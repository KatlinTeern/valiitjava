package ee.valiit;

import java.util.ArrayList;
import java.util.List;

enum Fuel {
    GAS, PETROL, DIESEL, HYBRID, ELECTRIC
}

public class Car {
    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;

    private Person owner;
    private Person driver;

    private int maxPassengers;
    private List<Person> passengers = new ArrayList<Person>();

    public List<Person> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Person> passengers) {
        this.passengers = passengers;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    // Konstruktor (constructor)
    // on eriline meetod, mis käivitatakse klassist objekti loomisel
    public Car() {
        System.out.println("Loodi auto objekt.");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    // Constructor overloading
    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, boolean isUsed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.isUsed = isUsed;
    }

    public void startEngine() {
        if (!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus.");
        } else {
            System.out.println("Mootor juba töötab.");
        }
    }

    public void stopEngine() {
        if (isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus.");
        } else {
            System.out.println("Mootor ei töötanudki.");
        }
    }

    public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada.");
        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti sõida.");
            System.out.printf("Auto maksimum kiirus on %d.%n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne.");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada madalamale kiirusele.");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d.%n", targetSpeed);
        }
    }

    // slowDown(int targetSpeed)

    public void slowDown(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada.");
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne.");
        } else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustada suuremale kiirusele.");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustas kuni kiiruseni %d.%n", targetSpeed);
        }
    }

    // park() mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid)
    // slowDown() + stopEngine()

    public void park() {
        slowDown(0);
        stopEngine();
        System.out.println("Auto pargitud.");
    }

    // Lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetodid

    // Loo mõni autoobjekt, millel on siis määratud kasutaja ja omanik
    // Prindi välja auto omaniku vanus (läbi auto)


    // Lisa autole max reisijate arv
    // Lisa autole võimalus hoida reisijaid
    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et et ei lisaks rohkem reisijaid kui mahub

    public void addPassengers(Person passenger) {
        if (passengers.size() < maxPassengers) {
            if (passengers.contains(passenger)) {
                System.out.printf("Autos juba on reisija %s%n", passenger.getFirstName());
            } else {
                passengers.add(passenger);
                System.out.printf("Autosse lisati reisija %s%n", passenger.getFirstName());
            }
        } else {
            System.out.println("Auto on täis.");
        }
    }

    public void removePassengers(Person passenger) {
        if (passengers.indexOf(passenger) != -1) {
            passengers.remove(passenger);
            System.out.printf("Autost eemaldati reisija %s%n", passenger.getFirstName());
        } else {
            System.out.println("Autos sellist reisijat ei ole.");
        }
    }

    public void showPassengers() {
        // Foreach loop
        // Iga elemendi kohta listis passengers tekita objekt passenger
        // 1. kordus Person passenger on esimene reisija
        // 2. kordus Person passenger on teine reisija
        System.out.println("Autos on järgmised reisijad:");
        for(Person passenger : passengers) {
            System.out.println(passenger.getFirstName());
        }
//        for (int i = 0; i < passengers.size(); i++) {
//            System.out.println(passengers.get(i).getFirstName());
//        }
    }



}
