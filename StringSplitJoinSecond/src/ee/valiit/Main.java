package ee.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";

	    // Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
        // | tähendab regulaaravaldises või
	    String[] words = sentence.split(" ja | |, ");

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        // join on staatiline meetod; välja saab kutsuda natuke teisiti: String.join
        // words on massiiv, sellel ei ole sellist meetodit nagu join

        String newSentence = String.join(" ", words);

        System.out.println(newSentence);

        newSentence = String.join(", ", words);

        System.out.println(newSentence);

        newSentence = String.join("\t", words);

        System.out.println(newSentence);

        // Escaping
        System.out.println("Juku\b\b\b\b \bütles:\\n \"Mulle meeldib suvi\"");
        System.out.println("C:\\Users\\opilane\\Documents");
        // vasakule poole kaldkriips on erisümbol, st vaja escape, kaks kaldkriipsu

        // Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale
        // eraldades tühikuga. Seejärel liidab need numbrid kokku ja prindib vastuse

        System.out.println();

        System.out.println("Sisesta arvud, mida tahad omavahel liita, eraldades tühikuga ");
        Scanner scanner = new Scanner(System.in);
        String numbersText = scanner.nextLine();

        String[] numbers = numbersText.split(" ");
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            // System.out.println(numbers[i]);
            sum += Integer.parseInt(numbers[i]);
        }

        String newNumbers = String.join(", ", numbers);

        System.out.printf("Arvude %s summa on %d%n", newNumbers, sum);

    }
}
