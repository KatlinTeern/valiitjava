package ee.valiit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikult ei ole,
            // siis tagastab see meetod null
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }

//            String line;
//            do {
//                line = bufferedReader.readLine();
//                if (line != null) {
//                    System.out.println(line);
//                }
//            } while (line != null);

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("Faili ei leitud.");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
