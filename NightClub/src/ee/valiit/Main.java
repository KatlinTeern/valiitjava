package ee.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Kõigepealt küsitakse külastaja nime
        // Kui nimi on listis, siis öeldakse kasutajale
        // "Tere tulemast, Kätlin"
        // ja küsitakse külastaja vanust
        // Kui kasutaja on alaealine, teavitata teda, et sorry
        // sisse ei pääse, muul juhul öeldakse "tere tulemast klubisse"

        // Kui kasutaja ei olnud listis
        // küsitakse kasutajalt ka tema perekonnanime
        // kui perekonnanimi on listis siis öeldakse "tere tulemast, Kätlin Teern"
        // muul juhul öeldakse "ma ei tunna sind"

        String firstName = "Kätlin";
        String lastName = "Teern";

        System.out.println("Mis on sinu eesnimi?");

        Scanner scanner = new Scanner(System.in);
        String userFirstName = scanner.nextLine();

        if (userFirstName.toLowerCase().equals(firstName.toLowerCase())) {
            System.out.println("Tere, " + userFirstName.toUpperCase());
            System.out.println("Kui vana sa oled?");
            int age = Integer.parseInt(scanner.nextLine());

            if (age < 18) {
                System.out.println("Kahjuks sisse ei saa.");
            }
            else {
                System.out.println("Tere tulemast klubisse!");
            }
        }
        else {
            System.out.println("Mis on sinu perekonnanimi?");
            String userLastName = scanner.nextLine();

            if (userLastName.toUpperCase().equals(lastName.toUpperCase())) {
                System.out.printf("Tere %sI sugulane!%n", firstName.toUpperCase());
            }
            else {
                System.out.println("Sind ei ole listis.");
            }
        }

    }
}
