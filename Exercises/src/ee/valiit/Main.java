package ee.valiit;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        // 1. Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala ekraanile

        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
        // ja mille sisendparameetriteks on kaks Stringi.
        // Meetod tagastab, kas tõene või vale selle kohta,
        // kas Stringid on võrdsed

        // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude
        // massiiv ja mis tagastab Stringide massiivi.
        // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis sama palju a tähti.
        // 3, 6, 7
        // "aaa", "aaaaaa", "aaaaaaa"

        // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
        // ja tagastab kõik sellel sajandil esinenud liigaastad.
        // Sisestada saab ainult aastaid vahemikus 500-2019. Ütle veateade, kui aastaarv ei mahu vahemikku.

        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega,
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // riikide nimekirja eraldades komaga.
        // tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning prndi välja
        // selle objekti toString() meetodi sisu.

        circleArea(5);
        sameWords("Kat", "Kat");

        int[] numbers = new int[] {3, 2, 7};
        String[] words = intArrayToStringArray(numbers);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        List<Integer> years = leapYearsInCentury(1897);

        for (int year : years) {
            System.out.println(year);
        }

        Language language = new Language();
        language.setLanguageName("Saksa");

        List<String> countries = new ArrayList<String>();
        countries.add("Saksamaa");
        countries.add("šveits");
        countries.add("Austria");

        language.setCountryList(countries);

        System.out.println(language);

    }

    public static void circleArea(double radius) {
        System.out.printf("Ringi pindala on: %.2f.%n", Math.PI * Math.pow(radius, 2));
    }

    public static boolean sameWords(String firstWord, String secondWord) {
        if (firstWord.equals(secondWord)) {
            System.out.println("Sõnad on samad.");
            return true;
        }
        System.out.println("Sõnad ei ole samad.");
        return false;
    }

    public static String[] intArrayToStringArray(int[] numbers) {
        String[] words = new String[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            words[i] = generateAString(numbers[i]);
        }
        return words;
    }

    private static String generateAString(int count) {
        String word = "";

        for (int i = 0; i < count; i++) {
            word += "a";
        }

        return word;
    }

    public static List<Integer> leapYearsInCentury(int year) {
        if (year < 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return new ArrayList<Integer>();
        }

        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        List<Integer> years = new ArrayList<Integer>();

        for (int i = 2020; i > centuryStart; i -= 4) {
            if (i <= centuryEnd && i % 4 == 0) {
                years.add(i);
            }
        }
        return years;
    }




}
