package ee.valiit;

import java.util.ArrayList;
import java.util.List;

public class Language {
    private String languageName;
    private List<String> countryList = new ArrayList<String>();



    public List<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<String> countryList) {
        this.countryList = countryList;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String toString() {
        return String.join(", ", countryList);
    }

}
