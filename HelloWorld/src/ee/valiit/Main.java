package ee.valiit;

// public - klass, meetod
//			või muutuja on avalikult mähtav/ligipääsetav
// class - javas üksus, üldiselt ka eraldi fail,
//			mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld - klassi nimi, mis on ka faili nimi
// static - meetodi ees tähenab, et seda meetodit saab
//			välja kutsuda ilma klassist objekti loomata
// void - meetod ei tagasta midagi
//			meetodile on võimalik kaasa anda parameetrid,
//			mis pannakse sulgude sisse, eraldades komaga
// String[] - tähistab stringi massiivi
// args - massiivi nimi, sisaldab käsurealt kaasa
//			pandud parameetreid
// System.out.println - on java meetod,
//			millega saab printida välja rida teksti
//			See, mis kirjutatakse sulgudesse, prinditakse välja
//			ja tehakse rea vahetus+-

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
