package ee.valiit;

import java.util.Scanner;

public class Main {

    // Küsi kasutajalt kaks arvu
    // Seejärel küsi kasutajalt, mis tehet ta soovib teha
    // liitmine, lahutamine, korrutamine, jagamine
    // Prindi kasutajale tehte vastus
    // Tee tsükliline kordus ka

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        boolean wrongAnswer;

        do {
            System.out.println("Siseta üks arv.");
            int a = Integer.parseInt(scanner.nextLine());

            System.out.println("Siseta teine arv.");
            int b = Integer.parseInt(scanner.nextLine());

            do {
                System.out.println("Vali tehe: \na liitmine, \nb lahutamine, \nc korrutamine, \nd jagamine ");
                String userChoice = scanner.nextLine();

                wrongAnswer = false;

                if (userChoice.equals("a")) {
                    System.out.printf("Arvude %d ja %d summa on %d.%n", a, b, sum(a, b));
                } else if (userChoice.equals("b")){
                    System.out.printf("Arvude %d ja %d lahutis on %d.%n", a, b, subtract(a, b));
                } else if (userChoice.equals("c")) {
                    System.out.printf("Arvude %d ja %d korrutis on %d.%n", a, b, multiply(a, b));
                } else if (userChoice.equals("d")){
                    System.out.printf("Arvude %d ja %d jagatis on %.2f.%n", a, b, divide(a, b));
                } else {
                    System.out.println("Selline valik puudub.");
                    wrongAnswer = true;
                }
            } while (wrongAnswer);

            System.out.println("Kas soovid jätkata? jah/ei");

        } while (scanner.nextLine().equals("jah"));



    }

    static int sum(int a, int b) {
        return a + b;
    }

    static int subtract(int a, int b) {
        return a - b;
    }

    static int multiply (int a, int b) {
        return a * b;
    }

    static double divide (int a, int b) {
        return (double)a / b;
    }




}
