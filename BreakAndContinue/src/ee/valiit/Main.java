package ee.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt pin-koodi
        // kui see on õige, siis ütleme "Tore"
        // muul juhul küsime uuesti.
        // Kokku küsime 3 korda.

        String realPin = "4545";

//        for (int i = 0; i < 3; i++) {
//
//            System.out.println("Palun siseta pin-kood.");
//
//            Scanner scanner = new Scanner(System.in);
//            String enteredPin = scanner.nextLine();
//
//            if (enteredPin.equals(realPin)) {
//                System.out.println("Tore! Pin-kood on õige.");
//                break;
//            } else {
//                System.out.println("Vale PIN.");
//            }
//        }


        Scanner scanner = new Scanner(System.in);
        int triesLeft = 3;

        do {
            System.out.println("Palun siseta pin-kood.");
            triesLeft--;

        } while (!scanner.nextLine().equals(realPin) && triesLeft > 0);

        if (triesLeft != 0) {
            System.out.println("Tore! Pin-kood on õige.");
        }

        System.out.println();

        // prindi välja 10 kuni 20 ja 40 kuni 60
        // continue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <= 60; i++) {
            if (i > 20 && i < 40) {
                continue;
            }
            System.out.println(i);
        }



    }
}
