package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    String sentence = "kevadel elutses Mutionu keset Kuuski noori vanu?";

        // Sümbolite indeksid tekstis algavad samamoodi indeksiga 0 nagu massiivides

        // Leia üles esimene tühik, mis on tema indeks

        int spaceIndex = sentence.indexOf(" ");
        // System.out.println(spaceIndex);
        // indexOf tagastab -1, kui otsitavat fraasi (sümbolit või sõna) ei leitud
        // ning indeksi (kust sõna algab) kui fraas leitakse

//        spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
//        System.out.println(spaceIndex);

        // Prindi välja kõigi tühikute indeksid lauses

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }

        System.out.println();

        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);
        }

        System.out.println();

        // Prindi lause esimesed 4 tähte
        String part = sentence.substring(0, 4);
        System.out.println(part);

        // Prindi lause esimene sõna
        spaceIndex = sentence.indexOf(" ");
        String firstWord = sentence.substring(0, spaceIndex);
        System.out.println(firstWord);

        // Teine sõna
        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        String secondWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(secondWord);

        // Leia esimene k-tähega algav sõna
        // et leiaks ka siis, kui see sõna on lause esimene sõna
        String firstLetter = sentence.substring(0, 1);
        String kWord = "";

        if (firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);
        }
        else {
            int kIndex = sentence.indexOf(" k") + 1;
            if (kIndex != 0) {
                spaceIndex = sentence.indexOf(" ", kIndex);
                kWord = sentence.substring(kIndex, spaceIndex);
            }
        }
        if (kWord.equals("")) {
            System.out.println("Lauses puudub k-ga algav sõna.");
        } else {
            System.out.println("Esimene k-ga algav sõna on " + kWord);
        }



        // Leia minu sõna mul lauses on

        System.out.println();

        spaceIndex = sentence.indexOf(" ");

        int spaceCounter = 0;

        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter + 1);


        // Leia mitu k-tähega algavat sõna on lauses

        System.out.println();

        int kCounter = 0;
        sentence = sentence.toLowerCase();

        int kIndex = sentence.indexOf(" k");
        while (kIndex !=-1) {
            kIndex = sentence.indexOf(" k", kIndex + 1);
            kCounter++;
        }

        if (sentence.substring(0, 1).equals("k")) {
            kCounter++;
        }

        System.out.printf("K-tähega algavate sõnade arv lauses on %d%n", kCounter);


    }
}
