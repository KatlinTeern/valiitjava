package ee.valiit;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    // Küsi kasutajalt 2 numbrit
        // prindi välja nende summa

        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisesta üks number.");
        int a = Integer.parseInt(scanner.nextLine());

        System.out.println("Sisesta teine number.");
        int b = Integer.parseInt(scanner.nextLine());

        // System.out.println("Sinu sisestatud numbrite summa on " + (a + b));
        System.out.printf("Numbrite %d ja %d summa on %d%n", a, b, a + b);

        System.out.println("Sisesta kolmas number.");
        double c = Double.parseDouble(scanner.nextLine());

        System.out.println("Sisesta neljas number.");
        double d = Double.parseDouble(scanner.nextLine());

        System.out.printf("Numbrite %.2f ja %.2f jagatis on %.2f%n", c, d, c / d);

        // Kui ma tahan nähe, 0,56 asemel 0.56
        // siis ütlen eraldi ette, et kasuta USA local-i
        System.out.println(String.format(Locale.US, "%.2f", c / d));

    }
}
