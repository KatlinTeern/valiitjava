package ee.valiit;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tektitüüpi (String) muutuja (variable)
        // mille nimeks paneme name ja väärtuseks Kätlin
        String firstName = "Kätlin";
        String lastName = "Teern";

        System.out.println("Hello " + firstName + " " + lastName + "!");
    }
}
