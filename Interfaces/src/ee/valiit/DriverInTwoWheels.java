package ee.valiit;

public interface DriverInTwoWheels extends Driver {
    void driveInRearWheel();
}
