package ee.valiit;

public class Car extends Vehicle implements Driver {
    private int maxDistance;
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Car() {
        this.maxDistance = 600;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Auto sõidab maanteel.");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Auto lõpetab sõitmise %d kilomeetri pärast.%n", afterDistance);
    }

    @Override
    public String toString() {
        return "Auto andmed: " + make + " " + maxDistance;
    }
}
