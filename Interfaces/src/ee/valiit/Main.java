package ee.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

        System.out.println();

        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);

        for (Flyer flyer: flyers) {
            flyer.fly();
            System.out.println();
        }

        Plane secondPlane = new Plane();
        secondPlane.getMaxDistance();
        secondPlane.drive();
        secondPlane.stopDriving(50);

        System.out.println();

        Car car = new Car();
        System.out.printf("See on auto maksimum distants: %d%n", car.getMaxDistance());
        car.drive();
        car.stopDriving(200);

        System.out.println();

        Motocycle motocycle = new Motocycle();
        System.out.printf("See on mootorratta maksimum distants: %d%n", motocycle.getMaxDistance());
        motocycle.drive();
        motocycle.driveInRearWheel();
        motocycle.stopDriving(10);
        motocycle.setSpeed(100);
        System.out.printf("Mootorratta kiirus on %d km/h.%n", motocycle.getSpeed());

        System.out.println();

        Car secondCar = new Car();
        secondCar.setMake("Audi");
        // see kutsub alati vaikimisi toStringi välja
        System.out.println(secondCar);

        System.out.println(boeing);


        // Lisa interface (liides) Driver, klass Car.
        // Mõtle, kas lennuk ja auto võiks mõlemad kasutada Driver liidest?
        // Driver liides võiks sisaldada 3 meetodi kirjeldust
        // int getMaxDistance()
        // void drive()
        // void stopDriving(int afterDistance) - saad määrata mitme meetri pärast lõpetab sõitmise
        // Pane auto ja lennuk mõlemad kasutama seda liidest
        // Lisa mootorratas

    }
}
