package ee.valiit;

public class Motocycle extends Vehicle implements DriverInTwoWheels {
    @Override
    public int getMaxDistance() {
        return 100;
    }

    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab ralli rajal.");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Mootorratas lõpetab sõitmise %d ringi pärast.%n", afterDistance);
    }

    @Override
    public void driveInRearWheel() {
        System.out.println("Mootorratas sõidab tagarattal.");
    }
}
