package ee.valiit;

// implements sunnib kasutama neid meetodeid, mis on interface´is kirjas

// Plane pärineb klassist Vehicle ja kasutab/implementeerib liidest
public class Plane extends Vehicle implements Flyer, Driver {

    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab.");
    }

    private void doCheckList() {
        System.out.println("Täidetakse checklist.");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus.");
    }

    @Override
    public int getMaxDistance() {
        System.out.println("See on lennuki maksimum distants.");
        return 0;
    }

    @Override
    public void drive() {
        System.out.println("Lennuk sõidab mööda hoovõturada.");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Lennuk jääb seisma %d meetri pärast.%n", afterDistance);
    }
}
