package ee.valiit;

public interface Driver {

    int getMaxDistance();
    void drive();
    void stopDriving(int afterDistance);

}
