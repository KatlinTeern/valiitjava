package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.WHITE;
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.AMOLED;

        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.BLACK;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.LCD;

        thirdMonitor.manufacturer = "Samsung";
        thirdMonitor.color = Color.GRAY;
        thirdMonitor.diagonal = 27;
        thirdMonitor.screenType = ScreenType.TFT;

        System.out.println(firstMonitor.manufacturer);
        System.out.println(secondMonitor.screenType);

        firstMonitor.color = Color.BLACK;

        int[] numbers = new int[3];

        Monitor[] monitors = new Monitor[4];

        // Lisa massiiivi 3 monitori
        // Prindi välja kõigi monitoride tootja, mille diagonaal on suurem kui 25 tolli

        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;

        monitors[3] = new Monitor();
        monitors[3].manufacturer = "Sony";
        monitors[3].color = Color.GRAY;
        monitors[3].diagonal = 21;
        monitors[3].screenType = ScreenType.LCD;

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > 25) {
                System.out.println(monitors[i].manufacturer);
            }
        }

        // Leia monitori värv kõige suuremal monitoril
        Monitor biggestMonitor = monitors[0];
        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > biggestMonitor.diagonal) {
                biggestMonitor = monitors[i];
            }
        }
        System.out.println("Suurim monitor on " + biggestMonitor.diagonal + " tolli ja selle värv on " + biggestMonitor.color);

        biggestMonitor.printInfo();
        firstMonitor.printInfo();
        secondMonitor.printInfo();

        System.out.printf("Monitori diagonaal cm-tes on %.2f.%n", firstMonitor.diagonalToCm());

    }
}
