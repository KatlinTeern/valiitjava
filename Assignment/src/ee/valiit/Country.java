package ee.valiit;

import java.util.List;

import static java.lang.String.format;

public class Country {
    private double population;
    private String countryName;
    private List<String> languages;

    public double getPopulation() {
        return population;
    }

    public void setPopulation(double population) {
        this.population = population;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        return String.format("Country: %s%nPopulation: %.1f million%nLanguages: %s%n",
                countryName, population, String.join(", ", languages));
    }
}
