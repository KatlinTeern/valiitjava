package ee.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    double number = Math.PI * 2;
        System.out.println(number);

        System.out.println();

        if(areEqual(5, 5)) {
            System.out.println("Numbrid on võrdsed.");
        }
        else {
            System.out.println("Numbrid ei ole võrdsed.");
        }

        System.out.println();

        String[] words = new String[] {"elas", "metsas", "mutionu", "keset", "kuuski"};
        int[] numbers = stringArrayToIntArray(words);
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        System.out.println(century(0));
        System.out.println(century(1));
        System.out.println(century(128));
        System.out.println(century(598));
        System.out.println(century(1624));
        System.out.println(century(1827));
        System.out.println(century(1996));
        System.out.println(century(2017));

        System.out.println();

        Country country = new Country();
        country.setCountryName("Estonia");
        country.setPopulation(1.5);

        List<String> languages = new ArrayList<String>();
        languages.add("Estonian");
        languages.add("Russian");

        country.setLanguages(languages);

        System.out.println(country);

    }

    static boolean areEqual(int firstNumber, int secondNumber) {
        if (firstNumber == secondNumber) {
            return true;
        }
        return false;
    }

    static int[] stringArrayToIntArray(String[] words) {
        int[] numbers = new int[words.length];

        for (int i = 0; i < numbers.length ; i++) {
            numbers[i] = words[i].length();
        }
        return  numbers;
    }

    static byte century(int year) {
        byte century = (byte) (year / 100 + 1);
        
        if (year < 1 || year > 2018) {
            return -1;
        }
        return century;
    }



}
