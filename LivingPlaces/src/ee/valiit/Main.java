package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    LivingPlace livingPlace = new Farm();

	    Pig pig = new Pig();
	    pig.setName("Kalle");

	    livingPlace.addAnimal(new Cat());
	    livingPlace.addAnimal(new Horse());
	    livingPlace.addAnimal(new Pig());
	    livingPlace.addAnimal(pig);

		Cow cow = new Cow();
		cow.setName("Priit");
	    livingPlace.addAnimal(cow);

	    livingPlace.printAnimalCounts();
	    livingPlace.removeAnimal("Cow");
		System.out.println();
		livingPlace.printAnimalCounts();

		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();

		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();

		System.out.println();

		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();

		// Mõelge ja täiedage zoo ja forest klasse nii, et neil oleks nende
		// 3 meetodi sisud

		System.out.println();

		LivingPlace secondLivingPlace = new Zoo();

		secondLivingPlace.addAnimal(new Elk());
		secondLivingPlace.addAnimal(new Elk());
		secondLivingPlace.addAnimal(new Elk());
		secondLivingPlace.addAnimal(new Pig());
		secondLivingPlace.addAnimal(new Horse());

		secondLivingPlace.printAnimalCounts();

		secondLivingPlace.removeAnimal("Elk");

		secondLivingPlace.printAnimalCounts();


		System.out.println();

		LivingPlace thirdLivingPlace = new Forest();

		thirdLivingPlace.addAnimal(new Fox());
		thirdLivingPlace.addAnimal(new Fox());
		thirdLivingPlace.addAnimal(new Fox());
		thirdLivingPlace.addAnimal(new Fox());
		thirdLivingPlace.addAnimal(new Elk());
		thirdLivingPlace.addAnimal(new Elk());
		thirdLivingPlace.addAnimal(new Elk());

		thirdLivingPlace.printAnimalCounts();

		thirdLivingPlace.removeAnimal("Elk");

		thirdLivingPlace.printAnimalCounts();



	}
}
