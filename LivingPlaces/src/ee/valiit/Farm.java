package ee.valiit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace {
    // ArrayList tegelt implements List
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Farm() {
        maxAnimalCounts.put("Pig", 5);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 15);
    }

    @Override
    public void addAnimal(Animal animal) {
        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if (!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmiloomad.");
            return;
        }

        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis ei ole sellistele loomadele kohti.");
            return;
        }

        if (animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on on kõik selle looma kohad täis.");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
        // Sellist looma veel ei ole farmis
        // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1);
        }

        animals.add((FarmAnimal) animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);
    }

    // Tee meetod, mis prindib välja kõik farmis elavad loomad ning mitu neid on

    @Override
    public void printAnimalCounts() {
        // Pig 2
        // Cow 3

        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    // Tee meetod, mis eemaldab farmist looma

    @Override
    public void removeAnimal(String animalType) {
        // "Pig"

        // Teen muutuja, mis ma tahan, et iga element listist oleks : list, mida tahan läbi käia
        // Farmanimal animal hakkab olema järjest esimene loom,
        // siis teine loom, kolmas ja seni kuni loomi on
        boolean animalFound = false;

        for (FarmAnimal animal : animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);

                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                // muuljuhul vähenda animalCounts mapis seda kogust

                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }

                animalFound = true;
                break; // kui break panna, siis nagu esimene leidis, siis kohe lõpetab
            }
        }

        if (!animalFound) {
            System.out.println("Farmis antud loom puudub.");
        }

//        for (int i = 0; i < animals.size(); i++) {
//            if (animals.get(i).getClass().getSimpleName().equals(animalType)) {
//                animals.remove(animals.get(i));
//            }
//        }

        // täienda meetodit nii, et kui ei leitud ühtegi sellest tüübist looma
        // prindi "Farmis selline loom puudub"


    }

}
