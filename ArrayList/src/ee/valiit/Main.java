package ee.valiit;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int[5];

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;

        // eemalda siit teine number
        numbers[1] = 0;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Tavalisse arraylisti võin lisada ükskõik mis tüüpi elemante
        // aga elemente küsides sealt pean ma teadma, mis tüüpi element kus
        // täpselt asub ning pean siis selleks tüübiks küsimisel ka cast-ima
        // teisendama
        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();

        list.add(money);
        list.add(random);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        int a = (int) list.get(1);
        String word = (String) list.get(0);

        int sum = 3 + (int)list.get(1);

        if (Integer.class.isInstance(a)) {
            System.out.println("a on int");
        }

        if (Integer.class.isInstance(list.get(1))) {
            System.out.println("listis indeksiga 1 on int");
        }

        if (Integer.class.isInstance(list.get(0))) {
            System.out.println("listis indeksiga 0 on String");
        }

        list.add(2, "head aega");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.23548);

        list.addAll(otherList);

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        if (list.contains(money)) {
            System.out.printf("money asub listis%n");
        }

        System.out.printf("money asub listis indeksiga %d%n", list.indexOf(money));
        System.out.printf("23 asub listis indeksiga %d%n", list.indexOf(44)); // kui ei leia, siis tagastab -1

        list.remove(money);

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println();
        System.out.println(list.size());

        list.remove(5);
        list.set(0, "tore");

        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }





    }
}
