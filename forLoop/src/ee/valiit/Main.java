package ee.valiit;

public class Main {

    public static void main(String[] args) {
	    // For tsükkel on selline tsükkel,
        // kus korduste arv on teada

        // Lõpmatu for tsükkel
//        for ( ; ; ) {
//            System.out.println("Väljas on ilus ilm.");
//        }

        // 1) int i = 0 =>
        // Siin saab luua muutujaid ja neid algväärtustada.
        // Luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma
        // 2) i < 3 => tingimus, mis peab olema tõena, et tsükkel käivituks ja korduks
        // 3) i++ => tegevus, mida iga tsükli korduse lõpus korratakse
        // i++ on sama mis i = i + 1
        // i-- on sama, mis i = i - 1;

        for (int i = 0; i < 3; i++) {
            System.out.println("Väljas on ilus ilm.");
        }

        System.out.println();

        // Prindi ekraanile numbrid 1 kuni 10
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }

        System.out.println();

        // Prindi ekraanile numbrid 24st 167ni
        // Prindi ekraanile numbrid 18st 3ni

        for (int i = 24; i <= 167; i++) {
            System.out.println(i);
        }

        System.out.println();

        for (int i = 18; i >= 3; i--) {
            System.out.println(i);
        }

        System.out.println();
        
        // Prindi ekraanile numbrid 2 4 6 8 10

        // i = i + 2 => i += 2
        // i = i - 4 => i -= 4
        // i = i ' 3 => i *= 3 arvu iseenda korrutamine mingi numbriga
        // i = i / 2 => i /= 2
        for (int i = 2; i <= 10; i+=2) {
            System.out.println(i);
        }

        System.out.println();

        // Prindi ekraanile numbrid 10 kuni 20 ja 40 kuni 60

        for (int i = 10; i <= 60; i++) {
            if (i <= 20 || i >= 40) {
                System.out.println(i);
            }
        }

        System.out.println();

        for (int i = 10; i <= 60; i++) {
            if (i == 21) {
               i = 40;
            }
            System.out.println(i);
        }

        System.out.println();

        // Prindi kõik arvud, mis jaguvad 3ga vahemikus 10 kuni 50
        // a % 3 annab jäägi
        // 4 % 3 => jääk 1
        // 6 % 3 => jääk 0
        // if (a % 3 == 0)

        for (int i = 10; i <= 50; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        System.out.println();

        // Paaritud arvud
        for (int i = 10; i <= 50; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }

    }
}
