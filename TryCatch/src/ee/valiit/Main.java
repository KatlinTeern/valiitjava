package ee.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int a = 0;
        try {
            int b = 4 / a;
            String word = null;
            word.length();
        }
        // Kui on mitu Catch plokki, siis otsib ta esimese ploki
        // mis oskab antud Exceptioni kinni püüda
        catch (ArithmeticException e) {
            if (e.getMessage().equals("/ by zero")) {
                System.out.println("Nulliga ei saa jagada.");
            } else {
                System.out.println("Esines aritmeetiline viga.");
            }
        }
        catch (RuntimeException e) {
            System.out.println("Esines reaalajas esinev viga.");
        }

        // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad.
        // Mis omakorda tähendab, et püüdes kinni
        // selle üldise Exceptioni, püüame kinni kõik Exceptionid
        catch (Exception e) {
            System.out.println("Esines viga.");
        }

        // Küsime kasutajalt numbri ja kui number ei ole korrektne, siis ütleme mingi veateate
        // st. ei ole nr nt. (sisend ei ole õiges firmaadis)

        Scanner scanner = new Scanner(System.in);
        boolean correctInput = false;

        do {
            System.out.println("Kirjuta üks number.");

            try {
                int number = Integer.parseInt(scanner.nextLine());
                correctInput = true;
            } catch (NumberFormatException e) {
                System.out.println("Sisend ei ole õiges formaadis.");
            }
        } while (!correctInput);

        // Loo täisarvude massiiv 5 täisarvuga ja ürita sinna lisada kuues täisarv
        // Näita veateadet

        int[] numbers = new int[] {5, 4, 2, 1, 3};
        try {
            numbers[5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Esines viga. Arvu asukoha indeks on piiridest väljas.");
        } catch (Exception e) {
            System.out.println("Juhtus mingi tundmatu viga.");
        }


    }
}
