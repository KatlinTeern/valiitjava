package ee.valiit;

public class Main {

    public static void main(String[] args) {

        // Muutujat deklareerida ja talle väärtust anda saab ka kahe sammuga.
	    String word;
	    word = "kala";

        int number;
        number = 3;

        int secondNumber = -7;

        System.out.println(number);
        System.out.println(secondNumber);

        // %n tekitab platvormi-spetsiifilise reavahetuse (Windows \r\n ja Linux/Mac \n)
        // selle asemel saab kasutada ka System.lineSeparator()
        System.out.println(number + secondNumber);
        System.out.printf("Arvude %d ja %d korrutis on %d%n", number, secondNumber, number * secondNumber);

        int a = 3;
        int b = 14;

        // String + number on alati string
        // Stringi liitmisel numbriga teisendatakse number stringiks ja liidetakse kui liitsõna

        System.out.println("Arvude summa on " + (a + b));
        System.out.printf("Arvude %d ja %d korrutis on %d%n", number, secondNumber, number + secondNumber);

        System.out.printf("Arvude %d ja %d jagatis on %d%n", a, b, a / b);
        System.out.printf("Arvude %d ja %d jagatis on %d%n", a, b, b / a);

        // Kahe täisarvu jagamisel on tulemuse jagatise täisosa ehk kõik peale koma süüakse ära

        int maxInt = 2147483647;
        int c = maxInt + 1;
        int d = maxInt + 2;
        System.out.println(c);
        System.out.println(d);

        int minInt = -2147483648;
        int e = minInt - 1;
        System.out.println(e);

        short f = 32199;

        // L-täht lõppu, kui longi kasutad
        long g = 800000000000L;
        long h = 234;

        System.out.println(g + h);

    }
}
