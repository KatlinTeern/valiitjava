package ee.valiit;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
//      While tsükkel on tsükkel, kus korduste arv ei ole teada
//      Lõpmatu tsükkel kasutades while
//	    while (true) {
//            System.out.println("Tere");
//        }

        // Programm mõtleb numbri
        // Programm ütleb kasutajale: "Arva number ära"
        // Senikaua kuni kasutaja arvab valesti, ütleb "Proovi uuesti"

        Random random = new Random();
        // random.nextInt(5) genereerib numbri 0 kuni 4
        int number = random.nextInt(5) + 1;
        // 80-100
        // int number = random.nextInt(20) + 80;

        // Teine võimalus sama asja teha
        // int randomNum = ThreadLocalRandom.current().nextInt(1, 6);

        System.out.println("Mõtlesin numbri 1 ja 5 vahel. Arva ära!");

        Scanner scanner = new Scanner(System.in);
        int enteredNumber = Integer.parseInt(scanner.nextLine());

        while (number != enteredNumber) {
            System.out.println("Proovi uuesti!");

            enteredNumber = Integer.parseInt(scanner.nextLine());

        }

        System.out.println("Tubli! Valitud number on " + enteredNumber);
    }
}
