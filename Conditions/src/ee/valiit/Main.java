package ee.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        //Type-safe language

        if (a == 3) {
            System.out.printf("Arv %d on võrdne kolmega.%n", a);
        }

        if (a < 5) {
            System.out.printf("Arv %d on väiksem kui viis.%n", a);
        }

        if (a != 4) {
            System.out.printf("Arv %d ei võrdu neljaga.%n", a);
        }

        if (a > 2) {
            System.out.printf("Arv %d on suurem kahest.%n", a);
        }

        if (a >= 7) {
            System.out.printf("Arv %d on suurem või võrdne seitsmega.%n", a);
        }

        if (!(a >= 7)) {
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega.%n", a);
        }

        //arv on 2 ja 8 vahel
        if (a > 2 && a < 8) {
            System.out.printf("Arv %d on 2 ja 8 vahel.%n", a);
        }

        // arv on väiksem kui 2 või suurem kui 8
        if (a < 2 || a > 8) {
            System.out.printf("Arv %d on väiksem kui 2 või suurem kui 8.%n", a);
        }

        // Kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
        if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10) {
            System.out.printf("Arv %d on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10.%n", a);
        }

        // Kui arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel
        // või arv on negatiivne aga pole suurem kui -14
        if (!(a > 4 && a < 6) && (a > 5 && a < 8) || a < 0 && !(a > -14)) {
            System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui 14", a);
        }

        // Kui tingimuste vahel on &&, siis kui üks tingimustest on vale, siis järgmisi tingimusi enam ei vaadata.
        // St. pane esimesse tingimusse kõige rohkem asju välistav tingimus.
        // Sama kehtib || puhul. Kui üks on õige, siis edasi enam ei vaadata.
        // St. esimesse tingimusse kõige tõenäolisem või rohkem asju kattev tingimus.

    }
}
