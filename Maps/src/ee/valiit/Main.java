package ee.valiit;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // siin hoitakse väärtuste paare (key => value)
        Map<String, String> map = new HashMap<String, String>();

        // Sõnaraamat
        // key => value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        // Oletame, et tahan teada, mis on inglise keeles Puu

        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>();
        idNumberName.put("32145698778", "Sander");
        idNumberName.put("46228523558", "Jaana");
        idNumberName.put("46228523558", "Tiina");
        idNumberName.put("32145698778", "Ott");

        // Kui kasutada put sama key lisamisel, kirjutatakse value üle
        // Key on unikaalne

        System.out.println(idNumberName.get("46228523558"));
        idNumberName.remove("46228523558");
        System.out.println(idNumberName.get("46228523558"));

        // EST => Estonia
        // Estonia => +372

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        // Char on selline tüüp, kus saab hoida üksikut sümbolit
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';

        Map<Character, Integer> lettersCount = new HashMap<Character, Integer>();
        String sentence = "elas metsas mutionu";

        char[] characters = sentence.toCharArray();


        for (int i = 0; i < characters.length; i++) {
            if (lettersCount.containsKey(characters[i])) {
               lettersCount.put(characters[i], lettersCount.get(characters[i]) + 1);
            } else {
               lettersCount.put(characters[i], 1);
            }
        }

        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is (või dictionarys)
        // ehk ühte key-value paari

        System.out.println(lettersCount);
        for (Map.Entry<Character, Integer> entry : lettersCount.entrySet()) {
            System.out.printf("Tähte %s esines %d korda.%n", entry.getKey(), entry.getValue());
        }




        // e 2
        // l 1
        // a 2
        // s 3

        // System.out.println(lettersCount.containsValue("e"));


    }
}
