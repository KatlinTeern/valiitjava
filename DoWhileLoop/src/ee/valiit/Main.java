package ee.valiit;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Do while tsükkel on nagu while tsükkel ainult et
        // kontroll tehakse peale esimest korda

//        System.out.println("Kas tahad jätkata? Jah/ei");
//        // Jätkame seni, kuni kasutaja kirjutab "ei"
//
//        Scanner scanner = new Scanner(System.in);
//        String answer = scanner.nextLine();
//
//        while (!answer.toLowerCase().equals("ei")) {
//            System.out.println("Kas soovid jätkata? Jah/ei");
//
//            userAnswer = scanner.nextLine();
//        }



        // Iga muutuja, mille me deklareerime, kehtib ainult
        // seda ümbritsevate {} see

//        do {
//            System.out.println("Kas tahad jätkata? Jah/ei");
//            answer = scanner.nextLine();
//        }
//        while (!answer.toLowerCase().equals("ei"));

        Scanner scanner = new Scanner(System.in);

        do {
            Random random = new Random();
            int number = random.nextInt(5) + 1;

            System.out.println("Mõtlesin numbri 1 ja 5 vahel. Arva ära!");
            int enteredNumber = Integer.parseInt(scanner.nextLine());

            while (number != enteredNumber) {
                System.out.println("Proovi uuesti!");
                enteredNumber = Integer.parseInt(scanner.nextLine());
            }

            System.out.println("Tubli! Valitud number on " + enteredNumber);
            System.out.println("Kas soovid jätkata?");
        }
        while (!scanner.nextLine().toLowerCase().equals("ei"));





    }
}
