package ee.valiit;

public class Main {

    public static void main(String[] args) {
	// Järjend, massiiv, nimekiri

        // Luuakse täisarvude massiiv, millesse mahub 5 elementi
        // Loomise hetkel määratud elementide arvu hiljem muuta ei saa.
        int[] numbers = new int[5];

        // Massiivi indeksid algavad 0st mitte 1st
        // viimane indeks on alati 1 võrra väiksem kui massiivi pikkus
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

        System.out.println(numbers[3]);

        System.out.println();

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        // Prindi numbrid tagurpidises järjekorras

        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        // Prindi numbrid, mis on suuremad kui 2

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }

        System.out.println();

        // Prindi kõik paarisarvud

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }

        System.out.println();

        // Prindi tagantpoolt 2 esimest paaritut arvu

        int counter = 0;
        for (int i = 4; i >= 0; i--) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if (counter ==2 ) {
                    break;
                }
            }
        }

        System.out.println();

        // Loo teine massiiv 3-le numbrile ja pane sinna esimesest
        // massiivist 3 esimest numbrit.
        // Prindi teise massiivi elemendid ekraanile.

        int[] secondNumbers = new int[3];

        for (int i = 0; i < secondNumbers.length; i++) {
            secondNumbers[i] = numbers[i];
        }

        for (int i = 0; i < secondNumbers.length; i++) {
            System.out.println(secondNumbers[i]);
        }

        System.out.println();

        // Loo kolmas massiiv 3-le numbrile ja pane sinna esimesest massiivist
        // 3 numbrit tagant poolt alates (1, 11, -2)

        int[] thirdNumbers = new int[3];

        for (int i = 0; i < thirdNumbers.length; i++) {
            thirdNumbers[i] = numbers[numbers.length - i -1];
        }

        for (int i = 0; i < thirdNumbers.length; i++) {
            System.out.println(thirdNumbers[i]);
        }

        System.out.println();

        int[] fourthNumbers = new int[3];

        for (int i = 0, j = numbers.length -1; i < fourthNumbers.length; i++, j-=2) {
            fourthNumbers[i] = numbers[j];
        }

        for (int i = 0; i < fourthNumbers.length; i++) {
            System.out.println(fourthNumbers[i]);
        }

    }
}
