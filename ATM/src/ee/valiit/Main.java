package ee.valiit;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {

    static String pin;

    public static void main(String[] args) {
        // Hoiame pin koodi failis pin.txt
        // ja kontojääki balance.txt

        int balance = loadBalance();
        pin = loadPin();
        String answer;
        int amount = 0;
        int count = 2;

        Scanner scanner = new Scanner(System.in);

        if (!correctPin()) {
            System.out.println("Kaart konfiskeeritud.");
            return;
        }

        do {
            chooseAction();
            answer = scanner.nextLine();
            switch (answer) {
                case "a":
                    transaction("Sularaha sissemakse");
                    break;
                case "b":
                    transaction("Sularaha väljamakse");
                    break;
                case "c":
                    loadBankStatement();
                    break;
                case "d":
                    System.out.printf("Kontojääk on %d€.%n", balance);
                    break;
                case "e":
                    changePIN();
                    pin = loadPin();
                    break;
                default:
                    System.out.println("Viga. Selline valik puudub.");
                    break;
            }

        } while (wantContinue());

    }


    static boolean correctPin() {
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Sisesta PIN kood:");

            String userPin = scanner.nextLine();

            if (userPin.equals(pin)) {
                System.out.println("PIN õige.");
                return true;
            } else {
                System.out.printf("Vale PIN. Kordi jäänud: %d%n", 2 - i);
            }
        }
        return false;
    }

    static void savePin(String pin) {
        // toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    static String loadPin() {
        // toimub pin välja lugemine pin.txt failist
        // Toimub shadowing
        // Shadowing refers to the practice in Java programming of using
        // two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope is hidden
        // because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.”
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;
    }

    static String changePIN() {
        System.out.println("Sisesta vana PIN kood:");
        Scanner scanner = new Scanner(System.in);
        String oldPin = scanner.nextLine();

        System.out.println("Sisesta uus PIN kood:");
        String pin = scanner.nextLine();

        System.out.println("Sisesta uus PIN uuesti:");
        String pin2 = scanner.nextLine();

        if (pin.equals(pin2)) {
            System.out.println("PIN kood kinnitatud");
            savePin(pin);
        }
        return pin;
    }

    static void chooseAction() {
        System.out.println("\nVali toiming:\n" +
                "a) Sularaha sissemakse\n" +
                "b) Sularaha väljamakse\n" +
                "c) Konto väljavõte\n" +
                "d) Kontojääk\n" +
                "e) Muuda PIN koodi");
    }

    static int chooseAmount() {
        System.out.println("\nVali summa:\n" +
                "a) 5\n" +
                "b) 10\n" +
                "c) 20\n" +
                "d) Muu summa");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        int amount = 0;

        switch (answer) {
            case "a":
                amount = -5;
                break;
            case "b":
                amount = -10;
                break;
            case "c":
                amount = -20;
                break;
            case "d":
                System.out.println("Sisesta summa: ");
                amount = -Integer.parseInt(scanner.nextLine());
                break;
            default:
                System.out.println("Selline valik puudub.");
                break;
        }
        return amount;
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }
    }

    static int loadBalance() {
        // toimub balance laadimine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

//    static void chooseDates() {
//        // current month
//        // current year
//        // choose dates
//        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEE");
//        Calendar calendar = Calendar.getInstance();
//        Date date = calendar.getTime();
//        System.out.println(dateFormat.format(date));
//    }

    static void saveBankStatement(String description, int amount, int balance) {
        try {
            FileWriter fileWriter = new FileWriter("bankstatement.txt", true);
            fileWriter.append(currentDateTimeToString()+ "\t" +
                    description + "\t\t" + amount + "\t\t" + balance + System.lineSeparator());

            // tyhi rida
            // kuupaev + total income + sum + total
            // kuupaev + total expenses + sum + total

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Andmete lisamine ebaõnnestus.");
        }
    }

    static String loadBankStatement() {
        String bankStatement = null;
        try {
            FileReader fileReader = new FileReader("bankstatement.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bankStatement= bufferedReader.readLine();
            while (bankStatement != null) {
                System.out.println(bankStatement);
                bankStatement = bufferedReader.readLine();
            }
            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("Konto väljavõtte laadimine ebaõnnestus");
        }
        return bankStatement;
    }

    static int transaction(String description) {
        int amount = 0;
        int balance = loadBalance();

        if (description.equals("Sularaha sissemakse")) {
            System.out.println("\nSisesta sissemakse summa.");
            try {
                Scanner scanner = new Scanner(System.in);
                amount = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Viga. Selline valik puudub.");
            }
            balance += amount;
            System.out.printf("Sularaha sissemakse: %d€.%n", amount);
        } else if (description.equals("Sularaha väljamakse")) {
            amount = chooseAmount();
            balance += amount;
            System.out.printf("Sularaha väljamakse: %d€.%n", amount);
        } else {
            System.out.println("Tehing ebaõnnestus.");
        }

        if (amount != 0) {
            saveBalance(balance);
            saveBankStatement(description, amount, balance);
        }
        return amount;
    }

    static boolean wantContinue() {
        boolean wantContinue = false;
        boolean correctAnswer;
        do {
            correctAnswer = true;
            System.out.println("\nKas soovid jätkata? jah/ei");
            Scanner scanner = new Scanner(System.in);
            String answer = scanner.nextLine();
            if (answer.equals("jah")) {
                wantContinue = true;
            } else if (answer.equals("ei")){
                System.out.println("Tänan! \nHead aega!");
            } else {
                System.out.println("Viga. Selline valik puudub.");
                correctAnswer = false;
            }
        } while (!correctAnswer);
        return wantContinue;
    }



}
